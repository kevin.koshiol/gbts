import { IOperation } from './ioperation'

export interface IOperationMap {
  [name: number]: IOperation
}
