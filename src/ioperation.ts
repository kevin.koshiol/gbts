export interface IOperation {
  cycles: number,
  action: () => any
}
